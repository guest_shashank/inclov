package com.ashu.inclov.utils;

import android.graphics.Bitmap;

/**
 * Created by Ashu on 8/3/2016.
 */

public class Const {

    public static final String TAG = "INCLOV";

    public static int splashInterval = 2000;

    public static int REQUEST_CODE_ASK_PERMISSIONS = 123;
    public static final int CAMERA_REQUEST = 1823;

    public static final String KEY_IMG_URI = "imageUri";
    public static final String KEY_U_NAME = "uName";
}
