package com.ashu.inclov.models;

import android.net.Uri;

/**
 * Created by Ashu on 8/5/2016.
 */

public class User {

    private String name;
    private long age;
    private int picture;
    private Uri updatedPicture;
    private String location;

    public User(String name, long age, int picture, Uri updatedPicture, String location) {
        this.name = name;
        this.age = age;
        this.picture = picture;
        this.updatedPicture = updatedPicture;
        this.location = location;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPicture() {
        return picture;
    }

    public void setPicture(int picture) {
        this.picture = picture;
    }

    public long getAge() {
        return age;
    }

    public void setAge(long age) {
        this.age = age;
    }

    public Uri getUpdatedPicture() {
        return updatedPicture;
    }

    public void setUpdatedPicture(Uri updatedPicture) {
        this.updatedPicture = updatedPicture;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
