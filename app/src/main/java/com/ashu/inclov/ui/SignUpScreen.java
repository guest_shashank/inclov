package com.ashu.inclov.ui;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatEditText;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.ashu.inclov.utils.Const;
import com.ashu.inclov.R;
import com.ashu.inclov.utils.Utils;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import de.hdodenhof.circleimageview.CircleImageView;

public class SignUpScreen extends AppCompatActivity implements View.OnClickListener {

    private Context context;
    private Button buttonSignUp;
    private CircleImageView circleImageView;
    private AppCompatEditText ed_name, ed_username, ed_password, ed_confirm_password;
    private AppCompatCheckBox checkBox_tnc;

    private Bitmap photo = null;
    private Uri uriSavedImage = null;
    private Boolean fromPicClick = false;
    private Uri resultUri = null;
    private String nName = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up_screen);
        context = SignUpScreen.this;
        initializeViews();
    }

    private void initializeViews() {

        if (getSupportActionBar() != null) {
            this.getSupportActionBar().setTitle(R.string.sign_up_title);
            this.getSupportActionBar().setHomeAsUpIndicator(android.R.drawable.ic_menu_close_clear_cancel);
            this.getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        }

        circleImageView = (CircleImageView) findViewById(R.id.profile_pic);
        buttonSignUp = (Button) findViewById(R.id.btn_signup);

        ed_name = (AppCompatEditText) findViewById(R.id.ed_name);
        ed_username = (AppCompatEditText) findViewById(R.id.ed_username);
        ed_password = (AppCompatEditText) findViewById(R.id.ed_password);
        ed_confirm_password = (AppCompatEditText) findViewById(R.id.ed_confirm_password);

        checkBox_tnc = (AppCompatCheckBox) findViewById(R.id.ch_tnc);

        circleImageView.setOnClickListener(this);
        buttonSignUp.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        int ch = view.getId();
        switch (ch) {
            case R.id.btn_signup:
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        checkCredentials();
                    }
                });
                break;

            case R.id.profile_pic:
                checkForCameraPermission();
                break;
        }
    }

    private void checkCredentials() {
        String uName = "";
        String pPass = "";
        String confirmPass = "";

        if (ed_name != null && !ed_name.getText().toString().equals("")) {
            nName = ed_name.getText().toString();
        } else {
            Utils.showToast(context, "Name Missing.");
        }

        if (ed_username != null && !ed_username.getText().toString().equals("")) {
            uName = ed_username.getText().toString();
        } else {
            Utils.showToast(context, "Username Missing.");
        }

        if (ed_password != null && !ed_password.getText().toString().equals("")) {
            pPass = ed_password.getText().toString();
        } else {
            Utils.showToast(context, "Password Missing.");
        }

        if (ed_confirm_password != null && !ed_confirm_password.getText().toString().equals("")) {
            confirmPass = ed_confirm_password.getText().toString();
        } else {
            Utils.showToast(context, "Confirm Password Missing.");
        }

        if (!pPass.equals("") && !confirmPass.equals("")) {
            if (pPass.equals(confirmPass)) {
                // success
                if (!uName.equals("") && !nName.equals("")) {
                    // success move to home.
                    if (checkBox_tnc.isChecked()) {
                        if (photo != null) {
                            moveToHome();
                            Utils.showToast(context, "Successfully Registered.");
                        } else {
                            Utils.showToast(context, "Profile pic is missing.");
                        }
                    } else {
                        Utils.showToast(context, "Please accepts Terms and Conditions.");
                    }
                }
            } else {
                Utils.showToast(context, "Passwords not matched.");
            }
        }
    }

    private void moveToHome() {
        Intent intent = new Intent(context, HomePage.class);
        intent.putExtra(Const.KEY_U_NAME, nName);
        intent.putExtra(Const.KEY_IMG_URI, resultUri.toString());
        startActivity(intent);
        finish();
    }

    private void checkForCameraPermission() {
        try {
            String[] permissions = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};

            //here check for runtime External Storage and Camera permission
            int hasStoragePermission = ContextCompat.checkSelfPermission(context,
                    permissions[0]);
            if (hasStoragePermission != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(SignUpScreen.this, permissions, Const.REQUEST_CODE_ASK_PERMISSIONS);
            }
            else {
                int hasCameraPermission = ContextCompat.checkSelfPermission(context,
                        permissions[1]);


                if (hasStoragePermission != PackageManager.PERMISSION_GRANTED &&
                        hasCameraPermission != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(SignUpScreen.this, permissions, Const.REQUEST_CODE_ASK_PERMISSIONS);

                } else {
                    setOnClickListener();
                    loadCamera();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setOnClickListener() {
        circleImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fromPicClick = true;
                loadCamera();
            }
        });

        fromPicClick = false;
    }

    private void loadCamera() {
        try {
            if (photo != null && !photo.isRecycled()) {
                photo.recycle();
            }
        } catch (Throwable t) {
            t.printStackTrace();
        }

        if (photo == null || fromPicClick) {
            try {
                File mediaStorageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);

                if (!mediaStorageDir.exists()) {
                    mediaStorageDir.mkdirs();
                }

                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmssSSS").format(new Date());
                File image = new File(mediaStorageDir.getPath(), File.separator + "IMG_" + timeStamp + ".jpg");
                uriSavedImage = Uri.fromFile(image);

                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                intent.putExtra("android.intent.extras.CAMERA_FACING", android.hardware.Camera.CameraInfo.CAMERA_FACING_FRONT);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, uriSavedImage);
                intent.putExtra("return-data", true);
                startActivityForResult(intent, Const.CAMERA_REQUEST);

            } catch (Exception e) {
                e.printStackTrace();
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, Const.CAMERA_REQUEST);
            }
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Const.CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {

            try {
                CropImage.activity(uriSavedImage)
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .setCropShape(CropImageView.CropShape.RECTANGLE)
                        .setAspectRatio(1, 1)
                        .setFixAspectRatio(true)
                        .start(this);

            } catch (Throwable e) {
                e.printStackTrace();
            }
        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {

            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                resultUri = result.getUri();

                if (resultUri != null) {
                    Log.d(Const.TAG, "_" + resultUri.getPath());
                    Bitmap myImg = BitmapFactory.decodeFile(resultUri.getPath());
                    Matrix matrix = new Matrix();
                    matrix.postRotate(90);
                    Bitmap rotated = Bitmap.createBitmap(myImg, 0, 0, myImg.getWidth(), myImg.getHeight(),
                            matrix, true);
                    photo = myImg;
                    circleImageView.setImageBitmap(myImg);
                }

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (photo != null && !photo.isRecycled()) {
            photo.recycle();
        }
    }
}
