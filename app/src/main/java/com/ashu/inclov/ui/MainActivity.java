package com.ashu.inclov.ui;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.ashu.inclov.R;
import com.ashu.inclov.utils.Utils;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button gPlus, fbLogin, emailLogin;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = MainActivity.this;
        initializeViews();
    }

    private void initializeViews() {
        gPlus = (Button) findViewById(R.id.sign_in_button);
        fbLogin = (Button) findViewById(R.id.login_button);
        emailLogin = (Button) findViewById(R.id.button);

        // set click listeners
        gPlus.setOnClickListener(this);
        fbLogin.setOnClickListener(this);
        emailLogin.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        int id = view.getId();
        switch (id) {
            case R.id.sign_in_button:
                Utils.showToast(context, getString(R.string.toast_msg));
                break;
            case R.id.login_button:
                Utils.showToast(context, getString(R.string.toast_msg));
                break;
            case R.id.button:
                startActivity(new Intent(context, LoginActivity.class));
                break;
        }
    }

}
