package com.ashu.inclov.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.ashu.inclov.utils.Const;
import com.ashu.inclov.R;

public class SplashScreen extends AppCompatActivity {

    private Context context;
    private Handler handler = null;
    private Runnable runnable = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        context = SplashScreen.this;
    }

    @Override
    protected void onPause() {

        if(handler != null && runnable != null)
        {
            handler.removeCallbacks(runnable);
            handler = null;
        }
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(handler == null)
        {
            handler = new Handler();

            runnable = new Runnable() {
                @Override
                public void run() {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            // start onBoarding Activity
                            startActivity(new Intent(context, MainActivity.class));
                            finish();
                        }
                    });
                }
            };

            handler.postDelayed(runnable,Const.splashInterval);
        }
    }
}
