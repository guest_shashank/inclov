package com.ashu.inclov.ui;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.ashu.inclov.R;
import com.ashu.inclov.adapter.SwipeDeckAdapter;
import com.ashu.inclov.models.User;
import com.ashu.inclov.utils.Const;
import com.ashu.inclov.utils.Utils;
import com.daprlabs.cardstack.SwipeDeck;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import de.hdodenhof.circleimageview.CircleImageView;

public class HomePage extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private SwipeDeck cardStack;
    private Context context;
    private Button buttonAddMore;

    private SwipeDeckAdapter adapter;
    private ArrayList<User> testData;

    private Uri img = null;
    private String nName = "UserName";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);

        context = HomePage.this;
        initializeViews();

        checkForCameraPermission();

        /*if (getIntent().getExtras() != null) {
            img = Uri.parse(getIntent().getExtras().getString(Const.KEY_IMG_URI));
            nName = getIntent().getExtras().getString(Const.KEY_U_NAME);
        }*/

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle(R.string.app_name);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);

        // get header layout
        View headerView = navigationView.getHeaderView(0);

        CircleImageView profileImage = (CircleImageView) headerView.findViewById(R.id.profilePic);
        TextView tvName = (TextView) headerView.findViewById(R.id.user_name);
        ImageView effect = (ImageView) headerView.findViewById(R.id.picEffect);
        Picasso.with(context).load(R.drawable.ic_contact_picture).fit().centerCrop().into(profileImage);
        Picasso.with(context).load(R.drawable.app_back).fit().centerCrop().into(effect);
        tvName.setText("shashank");
        tvName.setContentDescription("shashank");

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setContentDescription("Chat");

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Chat with your match.", Snackbar.LENGTH_SHORT)
                        .setAction("Action", null).show();
            }
        });

        cardStack.setEventCallback(new SwipeDeck.SwipeEventCallback() {
            @Override
            public void cardSwipedLeft(int position) {
                Log.i("MainActivity", "card was swiped left, position in adapter: " + position);
                Utils.showToast(context, "Dislike");
            }

            @Override
            public void cardSwipedRight(int position) {
                Log.i("MainActivity", "card was swiped right, position in adapter: " + position);
                Utils.showToast(context, "Like");
            }

            @Override
            public void cardsDepleted() {
                Log.i("MainActivity", "no more cards");
            }

            @Override
            public void cardActionDown() {
                Log.i(Const.TAG, "cardActionDown");
            }

            @Override
            public void cardActionUp() {
                Log.i(Const.TAG, "cardActionUp");
            }

        });

        buttonAddMore.setContentDescription("Tap for more profiles");
        buttonAddMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cardStack.setAdapter(adapter);
            }
        });
    }

    private void initializeViews() {

        cardStack = (SwipeDeck) findViewById(R.id.swipe_deck);
        cardStack.setHardwareAccelerationEnabled(true);

        buttonAddMore = (Button) findViewById(R.id.add_more_card);

        testData = new ArrayList<User>();
        testData.add(new User("Tony Stark", 24, R.drawable.app_back, null, "Mumbai"));
        testData.add(new User("Loki", 34, R.drawable.app_back, null, "Delhi"));
        testData.add(new User("Mr. D", 28, R.drawable.app_back, null, "UP"));
        testData.add(new User("Batman", 22, R.drawable.app_back, null, "Punjab"));
        testData.add(new User("Boom", 21, R.drawable.app_back, null, "Cape"));
        testData.add(new User("Stark", 30, R.drawable.app_back, null, "India"));
        testData.add(new User("Sliver", 22, R.drawable.app_back, null, "Gurgaon"));
        testData.add(new User("Tony D", 24, R.drawable.app_back, null, "Shimla"));
        testData.add(new User("Pinku", 21, R.drawable.app_back, null, "Bombay"));
        testData.add(new User("Rush", 30, R.drawable.app_back, null, "NY"));

        adapter = new SwipeDeckAdapter(testData, this, cardStack);
        cardStack.setAdapter(adapter);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home_page, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_notification) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            // Handle the camera action
        } else if (id == R.id.nav_filters) {

        } else if (id == R.id.nav_settings) {

        } else if (id == R.id.nav_about) {

        } else if (id == R.id.nav_contact) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void checkForCameraPermission() {
        try {
            String[] permissions = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};

            //here check for runtime External Storage and Camera permission
            int hasStoragePermission = ContextCompat.checkSelfPermission(context,
                    permissions[0]);
            if (hasStoragePermission != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(HomePage.this, permissions, Const.REQUEST_CODE_ASK_PERMISSIONS);
            } else {
                int hasCameraPermission = ContextCompat.checkSelfPermission(context,
                        permissions[1]);


                if (hasStoragePermission != PackageManager.PERMISSION_GRANTED &&
                        hasCameraPermission != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(HomePage.this, permissions, Const.REQUEST_CODE_ASK_PERMISSIONS);

                } else {
                    //setOnClickListener();
                    //loadCamera();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        adapter.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
