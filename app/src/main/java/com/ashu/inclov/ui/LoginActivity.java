package com.ashu.inclov.ui;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatEditText;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.ashu.inclov.R;
import com.ashu.inclov.utils.Utils;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView textViewSignUp;
    private Button buttonLogin;
    private Context context;
    private AppCompatEditText ed_username, ed_password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        context = LoginActivity.this;
        initializeViews();
    }

    private void initializeViews() {
        if (getSupportActionBar() != null) {
            this.getSupportActionBar().setTitle(R.string.login_title);
            this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        textViewSignUp = (TextView) findViewById(R.id.tv_signup);
        buttonLogin = (Button) findViewById(R.id.btn_login);
        ed_username = (AppCompatEditText) findViewById(R.id.ed_username);
        ed_password = (AppCompatEditText) findViewById(R.id.ed_password);

        textViewSignUp.setOnClickListener(this);
        buttonLogin.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        int ch = view.getId();
        switch (ch) {
            case R.id.btn_login:
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        checkCredentials();
                    }
                });
                break;

            case R.id.tv_signup:
                startActivity(new Intent(context, SignUpScreen.class));
                break;
        }
    }

    private void checkCredentials() {

        String uName = "";
        String pPass = "";

        if (ed_username != null && !ed_username.getText().toString().equals("")) {
            uName = ed_username.getText().toString();
        } else {
            Utils.showToast(context, "Username Missing.");
        }

        if (ed_password != null && !ed_password.getText().toString().equals("")) {
            pPass = ed_password.getText().toString();
        } else {
            Utils.showToast(context, "Password Missing.");
        }

        if (!uName.equals("") && !pPass.equals("")) {
            // success move to home.
            Utils.showToast(context, "Try Again.");
        }
    }
}
