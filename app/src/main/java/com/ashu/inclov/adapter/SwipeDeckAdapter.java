package com.ashu.inclov.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ashu.inclov.R;
import com.ashu.inclov.models.User;
import com.ashu.inclov.utils.Const;
import com.daprlabs.cardstack.SwipeDeck;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static android.app.Activity.RESULT_OK;

/**
 * Created by Ashu on 8/4/2016.
 */

public class SwipeDeckAdapter extends BaseAdapter {

    private ArrayList<User> dataUser;
    private Context context;
    private Bitmap photo = null;
    private Activity activity;
    private Uri uriSavedImage = null;
    private Uri resultUri = null;
    private User user;

    private View view;
    private ViewHolder holder;
    private SwipeDeck swipeDeck;

    public SwipeDeckAdapter(ArrayList<User> data, Context context, SwipeDeck swipeDeck) {
        this.dataUser = data;
        this.context = context;
        this.swipeDeck = swipeDeck;
        activity = (Activity) context;
    }

    @Override
    public int getCount() {
        return dataUser.size();
    }

    @Override
    public Object getItem(int position) {
        return dataUser.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        view = convertView;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            // normally use a viewholder
            view = inflater.inflate(R.layout.swipecard, parent, false);
            holder = new ViewHolder();
            holder.imageView = (ImageView) view.findViewById(R.id.card_image);
            holder.textView = (TextView) view.findViewById(R.id.card_user_name);
            holder.textView_age = (TextView) view.findViewById(R.id.card_user_age);
            holder.textView_loc = (TextView) view.findViewById(R.id.card_user_loc);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        user = dataUser.get(position);

        if (user.getUpdatedPicture() != null) {
            Picasso.with(context).load(user.getUpdatedPicture()).fit().centerCrop().into(holder.imageView);
        } else {
            Picasso.with(context).load(user.getPicture()).fit().centerCrop().into(holder.imageView);
        }

        holder.textView.setText(user.getName());
        holder.textView_age.setText(String.valueOf(user.getAge()));
        holder.textView_loc.setText(user.getLocation());

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("Layer type: ", Integer.toString(v.getLayerType()));
                Log.i("Hwardware Accel type:", Integer.toString(View.LAYER_TYPE_HARDWARE));
                //Utils.openAlertDialog(context, "Please Try Later. We are working on it.");
                loadCamera();
                view.setTag(position);
                Log.d(Const.TAG, "* " + position);
            }
        });
        return view;
    }

    private void loadCamera() {

        try {
            File mediaStorageDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);

            if (!mediaStorageDir.exists()) {
                mediaStorageDir.mkdirs();
            }

            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmssSSS").format(new Date());
            File image = new File(mediaStorageDir.getPath(), File.separator + "IMG_" + timeStamp + ".jpg");
            uriSavedImage = Uri.fromFile(image);

            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra("android.intent.extras.CAMERA_FACING", android.hardware.Camera.CameraInfo.CAMERA_FACING_FRONT);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, uriSavedImage);
            intent.putExtra("return-data", true);
            activity.startActivityForResult(intent, Const.CAMERA_REQUEST);

        } catch (Exception e) {
            e.printStackTrace();
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            activity.startActivityForResult(intent, Const.CAMERA_REQUEST);
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Const.CAMERA_REQUEST && resultCode == RESULT_OK) {

            try {
                CropImage.activity(uriSavedImage)
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .setCropShape(CropImageView.CropShape.RECTANGLE)
                        .setAspectRatio(1, 1)
                        .setFixAspectRatio(true)
                        .start(activity);

            } catch (Throwable e) {
                e.printStackTrace();
            }

        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {

            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                resultUri = result.getUri();

                if (resultUri != null) {
                    Log.d(Const.TAG, "_" + resultUri.getPath());
                    Bitmap myImg = BitmapFactory.decodeFile(resultUri.getPath());
                    Matrix matrix = new Matrix();
                    matrix.postRotate(90);
                    Bitmap rotated = Bitmap.createBitmap(myImg, 0, 0, myImg.getWidth(), myImg.getHeight(),
                            matrix, true);
                    photo = myImg;
                    //circleImageView.setImageBitmap(myImg);

                    int pos = (Integer) view.getTag();
                    Log.d(Const.TAG, "@" + pos);
                    dataUser.get(pos).setUpdatedPicture(resultUri);
                    Log.d(Const.TAG, "@user " + dataUser.get(pos).getName());

                    View currentView = swipeDeck.getChildAt(swipeDeck.getChildCount() - 1);
                    //Log.d(Const.TAG, "@" + pos + "_" + getCount() + "#" + swipeDeck.getChildCount());
                    if (currentView != null) {
                        ImageView imageView = (ImageView) currentView.findViewById(R.id.card_image);
                        Picasso.with(context).load(dataUser.get(pos).getUpdatedPicture()).fit().centerCrop().into(imageView);
                    }

                    notifyDataSetChanged();
                    resultUri = null;
                }

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }

    private static class ViewHolder {
        ImageView imageView;
        TextView textView;
        TextView textView_age;
        TextView textView_loc;
    }
}
