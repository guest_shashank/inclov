# README #

Module for Inclov android app.

This module includes few numbers of 3rd Party Lib --

   1. 'com.theartofdev.edmodo:android-image-cropper:2.2.+' (Image Cropping Lib) - used to crop image with 1:1 ratio.

   2. 'com.daprlabs.aaron:cardstack:0.3.0' (Tinder like Card) - used to show tinder like card on home screen.

   3. 'de.hdodenhof:circleimageview:2.0.0' (Circular image view) - used in profile section  both on drawer as well as on sign up screen.

   4. 'com.squareup.picasso:picasso:2.5.2' (Image cache lib) - used to set image to imageview and cache it to local storage.